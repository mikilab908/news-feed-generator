import React, { useState, useRef, useCallback } from 'react';
import useNewsSearch from '../useNewsSearch';

export default function News() {
  // setting the local state trough the React hooks
  const [query, setQuery] = useState('technology');
  const [searchText, setSearchText] = useState('');
  const [pageNumber, setPageNumber] = useState(1);

  // we are calling the "useNewsSearch hook" which returns news data array
  const { news, hasMore, loading, errorMessage } = useNewsSearch(
    query,
    pageNumber
  );

  // we are setting an observer to observe the last NEWS-DIV (node)
  // useREF hook give us the reference to the last element
  const observer = useRef();
  const lastNewsElementRef = useCallback(
    (node) => {
      if (loading) return;

      // disconect the observer from the prevous element
      // and attache it to the new last element
      if (observer.current) observer.current.disconnect();
      observer.current = new IntersectionObserver((entries) => {
        if (entries[0].isIntersecting && hasMore) {
          setPageNumber((prevPageNumber) => prevPageNumber + 1);
        }
      });
      if (node) observer.current.observe(node);
    },
    [loading, hasMore]
  );

  function handleSearch(e) {
    e.preventDefault();
    setQuery(searchText);
    setPageNumber(1);
    setSearchText('');
  }

  return (
    <div className='newsWrapper'>
      <div className='searchNews'>
        <form className='searchForm' onSubmit={handleSearch}>
          <input
            className='searchInput'
            type='text'
            placeholder='search for news'
            value={searchText}
            onChange={(e) => setSearchText(e.target.value)}
          />
          <button type='submit'>Search</button>
        </form>
        <div className='filterStories'>
          <button onClick={() => setQuery('sport')}>Sport</button>
          <button onClick={() => setQuery('science')}>Science</button>
          <button onClick={() => setQuery('nature')}>Nature</button>
        </div>
        <div>
          <h3>{errorMessage}</h3>
        </div>
        <div>
          <h3>{loading && 'Loading...'}</h3>
        </div>
      </div>
      <div className='allNews'>
        {/* rendering each news article */}
        {news.map((article, index) => {
          if (news.length === index + 1) {
            return (
              // the reference to the last news article in the array
              <div className='news' ref={lastNewsElementRef} key={article.url}>
                <img src={article.urlToImage} alt='' />
                <h1>{article.title}</h1>
                <p>{article.description}</p>
                <a href={article.url} target='_blank' rel='noreferrer'>
                  source{' '}
                </a>
              </div>
            );
          } else {
            return (
              <div className='news' key={article.url}>
                <img src={article.urlToImage} alt='' />
                <h1>{article.title}</h1>
                <p>{article.description}</p>
                <a href={article.url} target='_blank' rel='noreferrer'>
                  source{' '}
                </a>
              </div>
            );
          }
        })}
      </div>
    </div>
  );
}
