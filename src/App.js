import News from './components/News';
import './App.css';

export default function App() {
  return (
    <div className='App'>
      <News />
    </div>
  );
}
