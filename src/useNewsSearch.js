import { useEffect, useState } from 'react';
import axios from 'axios';

export default function useNewsSearch(query, pageNumber) {
  const [loading, setLoading] = useState(true);
  const [errorMessage, setErrorMessage] = useState('');
  const [news, setNews] = useState([]);
  const [hasMore, setHasMore] = useState(false);

  // every time we change query set news to empty array
  useEffect(() => {
    setNews([]);
  }, [query]);

  // trough the change of query and page number we are sending
  // an axios call for fatching the data from the NewsAPI
  useEffect(() => {
    setLoading(true);

    let cancel;

    axios({
      method: 'GET',
      url: 'https://newsapi.org/v2/everything',
      params: {
        q: query,
        page: pageNumber,
        pageSize: 20,
        apiKey: 'c83ff7fbfdb2436c945f6a65fa07b410',
      },

      // cancelation of the previous query, while typing the query
      // to not send the request until we finished the typing the keyword
      cancelToken: new axios.CancelToken((c) => (cancel = c)),
    })
      .then((response) => {
        setNews((prevNews) => {
          return [
            ...prevNews,
            ...response.data.articles.map((article) => article),
          ];
        });

        setHasMore(response.data.articles.length > 0);
        setLoading(false);
      })
      .catch((error) => {
        // return if we got and error because of the CANCELATION TOKEN
        if (axios.isCancel(error)) return;

        setErrorMessage(error.message);
      });
    return () => cancel();
  }, [query, pageNumber]);

  return { loading, news, hasMore, errorMessage };
}
